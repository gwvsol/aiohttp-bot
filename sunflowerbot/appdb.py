import mongoengine
from mongoengine import *
from pymongo.mongo_client import MongoClient
from pymongo.errors import ConnectionFailure, OperationFailure, ServerSelectionTimeoutError
from datetime import datetime
from json import loads
from .applog import *


class USERS(Document):
    """Документ с данными о пользователях"""
    userid              = LongField(required=True)
    username            = StringField(required=True)
    url                 = URLField(required=True)
    password            = StringField(default='')
    registration        = DateTimeField(required=True)   # Время регистрации
    registrationstr     = StringField(required=True)     # Время регистрации строка
    accesdate           = DateTimeField()                # Время одобрения
    accesdatestr        = StringField()                  # Время одобрения строка
    access              = BooleanField(required=True)
    privileges          = StringField(required=True)     # user/admin/new
    action              = ListField()                    # Поле со списком активности пользователя [1596564937, 1, 0, 17, 20, 1]

    meta = {'db_alias': 'db-bot'}


class MONGODB(object):
    
    def __init__(self, dbhost, dbport, dbname):
        self._dbname = dbname
        self._host   = dbhost
        self._port   = dbport
        self._dbconf = True if self._dbname and self._host and self._port else False
        self._mongoClient = False

        # Настраиваем логирование
        self.success    = success
        self.info       = info
        self.warning    = warning
        self.error      = error

    
        self.success("          [TeleBot-AppDb]")

    
    def open_connection(self):
        self.info("   [BOT-MONGODB] METHOD => open_connection")
        self._mongoClient = False
        if self._dbconf:
            self.currentConnection = \
                mongoengine.connect(db=self._dbname, host=self._host, port=self._port, \
                    alias="db-bot", connectTimeoutMS=5000, serverSelectionTimeoutMS=7000)
        if isinstance(self.currentConnection, MongoClient): 
            self._mongoClient = True
            self.check_connection()

    
    def check_connection(self):
        try:
            self.currentConnection.server_info()
            self._mongoClient = True
        except (ConnectionFailure, OperationFailure, ServerSelectionTimeoutError) as err:
            self._mongoClient = False
            self.error("  [BOT-MONGODB] ERROR: {}", f'{err}')
        self.info("  [BOT-MONGODB] METHOD => check_connection {}", self._mongoClient)
        return self._mongoClient


    def provide_connection(self):
        self.info("[BOT-MONGODB] METHOD => provide_connection")
        if not self._mongoClient: self.open_connection()
        else:
            if not self.check_connection():
                self.info("[BOT-MONGODB] METHOD => provide_connection => disconnect")
                mongoengine.disconnect("db-bot")
                self.open_connection()


    def GetUser(self, user_id=None, user_name=None):
        """Метод для запроса пользователя"""
        self.info("           [BOT-MONGODB] METHOD => GetUser <= {} {}", user_id, user_name)
        rcv = False, None, None, None
        self.provide_connection()
        if not self._mongoClient: 
            self.info("           [BOT-MONGODB] METHOD => GetUser {}", rcv)
            return rcv
        if user_id and user_name:
            try:
                user = USERS.objects(userid=user_id).get()
                rcv = user.access, user.userid, f'@{user.username}', user.url
            except mongoengine.DoesNotExist as ex:
                # Иначе данных в базе нет пользователя с таким id
                self.info("           [BOT-MONGODB] METHOD EXEPT => GetUser {} {} {}", user_id, user_name, f'{ex}')
            except mongoengine.connection.ConnectionFailure as ex:
                self.info("           [BOT-MONGODB] METHOD EXEPT => GetUser {}", f'{ex}')
        self.info("           [BOT-MONGODB] METHOD => GetUser {}", rcv)
        return rcv

    
    def GetUserAction(self, user_id=None, data=None):
        """Метод для получения активности пользователя"""
        self.info("    [BOT-MONGODB] METHOD => GetUserAction <= {} {}", user_id, data)
        rcv = False
        self.provide_connection()
        if not self._mongoClient: 
            self.info("    [BOT-MONGODB] METHOD => GetUserAction {}", rcv)
            return rcv
        if user_id:
            try:
                user = USERS.objects(userid=user_id).get()
                rcv = list(user.action)
            except mongoengine.DoesNotExist as ex:
                # Иначе данных в базе нет пользователя с таким id
                self.info("    [BOT-MONGODB] METHOD EXEPT => GetUserAction {} {}", user_id,f'{ex}')
            except mongoengine.connection.ConnectionFailure as ex:
                self.info("   [BOT-MONGODB] METHOD EXEPT => GetUserAction {}", f'{ex}')
        self.info("    [BOT-MONGODB] METHOD => GetUserAction {}", rcv)
        return rcv


    def UpdateUserAction(self, user_id=None, data=None):
        """Метод для обновления активности пользователя"""
        self.info(" [BOT-MONGODB] METHOD => UpdateUserAction <= {} {}", user_id, data)
        rcv = False
        self.provide_connection()
        if not self._mongoClient: 
            self.info(" [BOT-MONGODB] METHOD => UpdateUserAction {}", rcv)
            return rcv
        if user_id and isinstance(data, list):
            try:
                user = USERS.objects(userid=user_id).get()
                user.modify(
                    action = data 
                    )
                # Загружаем обновленный документ из Базы Данных
                user.reload()
                rcv = user.action
            except mongoengine.DoesNotExist as ex:
                # Иначе данных в базе нет пользователя с таким id
                self.info(" [BOT-MONGODB] METHOD EXEPT => UpdateUserAction {} {}", user_id,f'{ex}')
            except mongoengine.connection.ConnectionFailure as ex:
                self.info("[BOT-MONGODB] METHOD EXEPT => UpdateUserAction {}", f'{ex}')
        self.info(" [BOT-MONGODB] METHOD => UpdateUserAction {}", rcv)
        return rcv


    def CreateUser(self, user_id=None, user_name=None):
        """Метод для создания пользователя"""
        self.info("        [BOT-MONGODB] METHOD => CreateUser <= {} {}", user_id, user_name)
        rcv = False, None, None, None
        self.provide_connection()
        if not self._mongoClient: 
            self.info("        [BOT-MONGODB] METHOD => CreateUser {}", rcv)
            return rcv
        if user_id and user_name:
            try:
                # Создаем нового пользователя в базе данных
                user = USERS(
                    userid          = user_id,
                    username        = user_name,
                    url             = f'https://t.me/{user_name}',
                    access          = False,
                    registration    = datetime.utcnow(),
                    registrationstr = datetime.now().strftime("%Y:%m:%d %H:%M:%S"),
                    privileges      = 'new').save()
                user.reload()
                rcv = user.access, user.userid, f'@{user.username}', user.url
            except mongoengine.connection.ConnectionFailure as ex:
                self.info("        [BOT-MONGODB] METHOD EXEPT => CreateUser {}", f'{ex}')
        self.info("       [BOT-MONGODB] METHOD => CreateUser {}", rcv)
        return rcv