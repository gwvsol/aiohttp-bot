from loguru import logger

# Настраиваем логирование
logger.add("appbot.log", \
    format="<green>{time:YYYY-MM-DD HH:mm:ss}</green> | {level} | <cyan>{name}</cyan>:<cyan>{function}</cyan>:<cyan>{line}</cyan> {message}", \
    rotation="00:00", compression="zip", enqueue=True) #"appbot-{time}.log" , diagnose=False
success    = logger.success
info       = logger.info
warning    = logger.warning
error      = logger.error

success("         [TeleBot-AppLog]")