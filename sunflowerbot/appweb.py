# -*- coding: utf-8 -*-
import ssl
from aiohttp import web
from .setting import config
from .appbot import *
from .routes import setup_routes


"""Запуск приложения Telegram Bot"""
def main():
    # Создаем веб проиложение
    app = web.Application()
    # пока не знаю, нужно ли будет в нем дополнительные настройки
    #app['config'] = config
    # добавляем маршрут для обработки запросов от API Telegram
    setup_routes(app)

    # Добавляем обработку ssl сертификатов
    context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
    context.load_cert_chain(config.get('cert'), config.get('pkey'))

    success("             [TeleBot-AppWeb]")
    # Стартуем aiohttp приложение
    web.run_app(
        app,
        host=config.get('listen'),
        port=config.get('port'),
        ssl_context=context, )


if __name__ == "__main__":
    main()