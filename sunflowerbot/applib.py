import time
from copy import deepcopy
from .setting import config
from .appdb import MONGODB
from .applog import *


# Создаем подключение в базе данных
db = MONGODB(config.get('dbhost'), config.get('dbport'), config.get('dbname'))

# Кортедж со временем для выбора
clocks = ('8:00', '9:00', '10:00', '11:00', '12:00', '13:00', '14:00', \
              '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00')
# Кортеж с днями недели
wdays =   ('Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс')
# Кортедж выбора варианта работы
optionWork = ('Ищу', 'Предоставляю')
# Кортедж выбора недели
week = ('Текущая', 'Следующая')
# Кортедж главного меню
mainMenu = ('start', 'Вход', 'Главное меню')
success("         [TeleBot-AppLib]")

dbBot = dict() 


def CheckWdays(day):
    """Метод для получения индекса дня недели"""
    try: 
        return wdays.index(str(day))
    except ValueError: 
        return 100


def CheckClocks(cl):
    """Метод для получения времени из кортеджа"""
    try:
        clockIndex = clocks.index(str(cl))
        return clockIndex + 8
    except ValueError: 
        return 0


def MenuTime(clock=None):
    """Метод для создания динамического меню выбра времени начала"""
    # success("   [TeleBot-AppLib] clock: {}", clock)
    if isinstance(clock, int): 
        success("   [TeleBot-AppLib] clock: {}", list(time.localtime(clock))[:-2])
        #success("   [TeleBot-AppLib] dbBot: {}", dbBot)
        return list(clocks)
    elif isinstance(clock, str):
        indexClock = CheckClocks(clock) - 7
        return [clocks[i] for i in range(indexClock, 14)]


def UpdateDb(user_id=None, message=None, data=None):
    """Метод для обновления данные о действиях пользователя"""
    success("[TeleBot-AppLib] ID: {} MESSAGE: {}", user_id, message)
    if user_id:
        userAction = db.GetUserAction(user_id=user_id)
        userActionAll = deepcopy(userAction)
        #success("[TeleBot-AppLib] UpdateDb => userActionAll: {}", userActionAll)
        # Обработка команды Главное меню
        if message == mainMenu[2]: 
            if len(userActionAll) > 0:
                if len(userActionAll[-1]) < 6: 
                    userActionAll[-1] = list([data])
                elif len(userActionAll[-1]) == 6:
                    userActionAll.append(list([data]))
            else: userActionAll.append(list([data]))
        elif message == week[0]: 
            #success("[TeleBot-AppLib] UpdateDb => WEEK: {}", message)
            if len(userActionAll[-1]) < 6: 
                userActionAll[-1] = list([data])
            userActionAll[-1].append(0)
        elif message == week[1]:
            #success("[TeleBot-AppLib] UpdateDb => WEEK: {}", message)
            if len(userActionAll[-1]) < 6: 
                userActionAll[-1] = list([data])
            userActionAll[-1].append(1)
        elif message == optionWork[0]:
            #success("[TeleBot-AppLib] UpdateDb => OPTION-WORK: {}", message)
            userActionAll[-1].append(0) 
        elif message == optionWork[1]:
            #success("[TeleBot-AppLib] UpdateDb => OPTION-WORK: {}", message)
            userActionAll[-1].append(1) 
        elif CheckWdays(message) <= 6:
            #success("[TeleBot-AppLib] UpdateDb => WDAYS: {}", CheckWdays(message))
            userActionAll[-1].append(CheckWdays(message))
        elif CheckClocks(message) >= 8 and CheckClocks(message) <= 22:
            #success("[TeleBot-AppLib] UpdateDb => CLOCKS: {}", CheckClocks(message))
            userActionAll[-1].append(CheckClocks(message))
        success("[TeleBot-AppLib] userActionAll: {}", userActionAll)
        db.UpdateUserAction(user_id=user_id, data=userActionAll)


def CheckUser(user_id=None, user_name=None):
    """Метод для проверки имеется ли пользователь в базе данных, а так же 
       создания нового пользователя если его нет в базе данных"""
    access, userid, username, url = False, None, None, None
    if user_id and user_name:
        access, userid, username, url = \
            db.GetUser(user_id=user_id, user_name=user_name)
        if not access and username is None:
            access, userid, username, url = \
                db.CreateUser(user_id=user_id, user_name=user_name)
    return access, userid, username, url


def GetMenu(m):
    """Метод для вывода меню, если он передан в параметрах метода"""
    rcv = None
    for w in mainMenu[1:]:
        if w == str(m): rcv = w
    return rcv


def Wday():
    """Метод для вывода текущего деня недели цифрой"""
    rcv = time.localtime().tm_wday
    return rcv


def DayStr(d):
    """Метод для вывода дня недели, если он передан в параметрах метода"""
    rcv = None
    for w in wdays:
        if w == str(d): rcv = w
    return rcv


def WeekStr(w):
    """Метод для вывода недели, если он передан в параметрах метода"""
    rcv = None
    for n in week:
        if n == str(w): rcv = n
    return rcv


def TimeStart(user_id, text, stop):
    """Метод для вывода времени, если этот параметр передан"""
    success("[TeleBot-AppLib] TimeStart: {} {} {}", user_id, text, stop)
    userAction = db.GetUserAction(user_id=user_id)
    success("[TeleBot-AppLib] TimeStart => userAction: {}", userAction)
    rcv = None
    if isinstance(userAction[-1], list) and len(userAction[-1]) == stop:
        for n in clocks:
            if n == str(text): rcv = n
    success("[TeleBot-AppLib] TimeStart => rcv: {}", rcv)
    return rcv


def OptinStr(o):
    """Метод для вывода вариантов работы, если этот параметр передан"""
    rcv = None
    for n in optionWork:
        if n == str(o): rcv = n
    return rcv


def WdayStr(day=None):
    """Метод вывода дня недели, если нет параметра,
       выводится текущий день недели"""
    # http://python.su/forum/topic/23220/
    if day is None: day = Wday()
    if isinstance(day, int) and day <= 6: return wdays[day]
    else: return None


def WMenu(wnext=False):
    """Создание меню дней недели, при wnext=True
       выводится вся неделя полностью"""
    if wnext: w = 0
    else: w = Wday()
    return [WdayStr(i) for i in range(w, 7)]