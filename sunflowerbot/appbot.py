import telebot
# import time
from datetime import datetime
from telebot import types
from .setting import config
from .applib import *
from .applog import *

success("          [TeleBot-AppBot]")

# Создаем Telegram бота
bot = telebot.TeleBot(config.get('token'))


def SendToDev(user_id, menu, text, row):
    """Метод для формирования данных и отправки их на девайс пользователя"""
    markup = types.ReplyKeyboardMarkup(True, True, row_width=row)
    markup.add(*menu)
    bot.send_message(user_id, text, reply_markup=markup)
    #success("  [TeleBot-AppBot] DBBot: {}", dbBot)


def CheckSendToDev(dbdata, menu, text, row):
    """Метод для проверки имеет ли пользователь право работы с ботом"""
    txt = f"Извините,\nвы не имеете разрешения\nid: {dbdata[1]}\nusername: {dbdata[2]}\n"
    txt1 = f"Извините, \nу вас не верно настроен\nмессенджер\nid: {dbdata[1]}\nusername: {dbdata[2]}\n"
    if dbdata[0] and dbdata[2]: SendToDev(user_id=dbdata[1], menu=menu, text=text, row=row)
    elif not dbdata[0] and not dbdata[2]: SendToDev(user_id=dbdata[1], menu=[mainMenu[1]], text=txt1, row=row)
    else: SendToDev(user_id=dbdata[1], menu=[mainMenu[1]], text=txt, row=1)


@bot.message_handler(commands=[mainMenu[0]])
def Welcome(message):
    """Обработка команды старт
       с этой команды начинается работа с ботом"""
    txt="Пожалуйста выполните вход"
    SendToDev(user_id=message.from_user.id, menu=[mainMenu[1]], text=txt, row=1)


@bot.message_handler(func=lambda message: GetMenu(message.text) == message.text)
def MainMenu(message):
    """Обработка команд Вход и Главное меню выводится меню выбора Текущей и Следующей недели,
       а так же возврата в Главное меню"""
    db = CheckUser(user_id=message.from_user.id, user_name=message.from_user.username)
    if not db[0]: db = [db[0], message.from_user.id, message.from_user.username, "None"]
    txt = f"Выбирите неделю для поиска"
    menu = list(week)
    menu.append(mainMenu[2])
    UpdateDb(user_id=message.from_user.id, message=message.text, data=message.date)
    CheckSendToDev(dbdata=db, menu=menu, text=txt, row=2)


@bot.message_handler(func=lambda message: WeekStr(message.text) == message.text)
def WeeklyMenu(message):
    """Обработка команд Текущая и Следующая нелели
       Вывод меню недели, если текущая неделя, выводятся дни с текущего дня, прошедшие дни не выводятся
       Меню следующей недели выводится полностью, кроме этого выводится меню возврата в Главное меню"""
    db = CheckUser(user_id=message.from_user.id, user_name=message.from_user.username)
    txt=f"Выбирите день недели"
    if week[0] == message.text: menu = WMenu()
    elif week[1] == message.text: menu = WMenu(wnext=True)
    menu.append(mainMenu[2])
    UpdateDb(user_id=message.from_user.id, message=message.text, data=message.date)
    CheckSendToDev(dbdata=db, menu=menu, text=txt, row=3)


@bot.message_handler(func=lambda message: DayStr(message.text) == message.text)
def TimeDayMenu(message):
    """Обработка команд выбора времени начала" 
       кроме этого выводится меню возврата в Главное меню"""
    db = CheckUser(user_id=message.from_user.id, user_name=message.from_user.username)
    txt=f"Выбирите время начала"
    #success("[TeleBot-AppBot] unux time: {}", message.date)
    #success("[TeleBot-AppBot] str time:  {}", datetime.fromtimestamp(message.date).strftime("%Y-%m-%d %H:%M:%S"))
    #success("[TeleBot-AppBot] list time: {}", list(time.localtime(message.date))[:-2])
    menu = MenuTime(message.date)
    menu.append(mainMenu[2])
    UpdateDb(user_id=message.from_user.id, message=message.text)
    CheckSendToDev(dbdata=db, menu=menu, text=txt, row=4)


@bot.message_handler(func=lambda message: TimeStart(message.from_user.id, message.text, stop=3) == message.text)
def StartTime(message):
    """Обработка команд выбора времени начала" 
       кроме этого выводится меню возврата в Главное меню"""
    db = CheckUser(user_id=message.from_user.id, user_name=message.from_user.username)
    txt=f"Выбирите время завершения"
    menu = MenuTime(message.text)
    menu.append(mainMenu[2])
    UpdateDb(user_id=message.from_user.id, message=message.text)
    CheckSendToDev(dbdata=db, menu=menu, text=txt, row=4)


@bot.message_handler(func=lambda message: TimeStart(message.from_user.id, message.text, stop=4) == message.text)
def StopTime(message):
    """Обработка команд выбора времени завершения" 
       кроме этого выводится меню возврата в Главное меню"""
    db = CheckUser(user_id=message.from_user.id, user_name=message.from_user.username)
    txt=f"Вы в поиске?\nПредоставляете свое время?"
    menu = list(optionWork)
    menu.append(mainMenu[2])
    UpdateDb(user_id=message.from_user.id, message=message.text)
    CheckSendToDev(dbdata=db, menu=menu, text=txt, row=2)


@bot.message_handler(func=lambda message: OptinStr(message.text) == message.text)
def OptionWork(message):
    """Метод для обработки выбора работы"""
    db = CheckUser(user_id=message.from_user.id, user_name=message.from_user.username)
    if message.text == optionWork[0]: txt=f"Выполняем поиск доступных вариантов"
    else: txt=f"Ваша заявка зарегистрирована"
    cleartxt = "НЕ ЗАБУДЬТЕ ОЧИСТИТЬ ИСТОРИЮ"
    txt = f'{txt}\n{cleartxt}'
    menu = [mainMenu[2]]
    UpdateDb(user_id=message.from_user.id, message=message.text)
    CheckSendToDev(dbdata=db, menu=menu, text=txt, row=1)


# Удалить webhook, это иногда не удается установить, если есть предыдущий webhook
bot.remove_webhook()

# Настраиваем webhook
bot.set_webhook(url=config.get('url_base') + config.get('url_path'),
                certificate=open(config.get('cert'), 'r'))
