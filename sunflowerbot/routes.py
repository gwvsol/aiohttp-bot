from .views import handle

# Добавляем маршрут для обработки запросов от API Telegram 
def setup_routes(app):
   app.router.add_post('/{token}/', handle)