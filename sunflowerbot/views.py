import telebot
from aiohttp import web
from .appbot import bot

# Обработка в асинхронном режиме webhook вызовов от API Telegram
async def handle(request):
    if request.match_info.get('token') == bot.token:
        request_body_dict = await request.json()
        update = telebot.types.Update.de_json(request_body_dict)
        bot.process_new_updates([update])
        return web.Response()
    else:
        return web.Response(status=403)