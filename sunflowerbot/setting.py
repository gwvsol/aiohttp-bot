import pathlib
import yaml

# Основной словарь с настройками
config = dict()
CONFIG_DIR='config' # Директория где располагается файл настроек
CERT_DIR='cert'     # Директория с ssl сертификатами
CONFIG_FILE='config-new.yaml'   # Файл настроек
PORT=8443                   # Порт на котором будет работать сервис
LISTEN_IP='0.0.0.0'         # Какой IP адрес будет слушать приложение
BASE_DIR = pathlib.Path(__file__).parent.parent
CONFIG = BASE_DIR/CONFIG_DIR/CONFIG_FILE

# Метод для чтения файла настроек
def get_config(path):
    with open(path) as f:
        cfile = yaml.safe_load(f)
    return cfile

# Получаем данный из файла настроек
conf = get_config(CONFIG).get('config')
# Полный путь к файлам ssl сертификатов
cert = BASE_DIR/CERT_DIR/conf.get('cert')
pkey = BASE_DIR/CERT_DIR/conf.get('pkey')
# Добавляем в словарь настроек данные из файла настроек
config.update(conf)
# Обновляем и добавляем информацию в словарь настроек
config.update(cert=cert, pkey=pkey)
config.update(port=PORT, listen=LISTEN_IP)
config.update(url_base=f'https://{config.get("host")}:{config.get("port")}')
config.update(url_path=f'/{config.get("token")}/')