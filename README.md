### Telegram Bot SunFlowerSearch

Telegram Bot to help you find free time from others

---

#### Запуск Telegram Bot

```make prepare```	    - Подготовка виртуального окружения к установке проекта   
```make install```	    - Установка проекта  
```make run```	        - Запуск бота в процессе разработки в виртуальном окружении   
```make clean```	    - Очистка проекта от файлов созданных утилитой distutils  
```make clean-pyc```	- Удаление файлов, созданных интерпретатором Python  
```make install```	    - Сборка проекта  

---

#### Установка сервиса



---

#### Записи по разработке приложения

```python
    print(message.message_id)
    print(message.from_user.id)
    print(message.from_user.first_name)
    print(datetime.fromtimestamp(message.date).strftime('%Y-%m-%d %H:%M:%S'))
    print(message.date)
    print(message.chat.id)
    print(message.chat.username)
    print(message.text)
```  