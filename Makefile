.PHONY: help prepare venv install run clean clean-pyc

#============================================
SOURCE=sunflowerbot
#============================================
VENV_NAME?=venv
VENV_ACTIVATE=. ${VENV_NAME}/bin/activate
PYTHON=${VENV_NAME}/bin/python3
#============================================
MONGOIMAGE=mongo
DOMAIN=srv01.hldns.ru
MONGODB=sunflowerbot-db
DBDIR=/opt/data
CERTDIR=cert
MONGODBDIR=${DBDIR}/${SOURCE}
USER=$(shell whoami)
RELEASE=release
#============================================

.DEFAULT: help

help:
	@echo "make prepare	- Подготовка виртуального окружения к установке проекта"
	@echo "make install	- Установка проекта"
	@echo "make run	- Запуск бота в процессе разработки в виртуальном окружении"
	@echo "make clean	- Очистка проекта от файлов созданных утилитой distutils"
	@echo "make clean-pyc	- Удаление файлов, созданных интерпретатором Python"
	@echo "make install	- Соборка проекта"
	@exit 0

prepare:
	# sudo apt update
	# sudo apt upgrade -y
	sudo apt install -y python3 python3-pip python3-dev python3-setuptools
	sudo apt install -y build-essential libssl-dev libffi-dev openssl
	sudo pip3 install virtualenv
	make venv


build-db:
	docker pull ${MONGOIMAGE}
	[ -d ${MONGODBDIR} ] || sudo mkdir -p ${MONGODBDIR}
	! [ `ls -l ${DBDIR} | grep ${SOURCE} | awk '{print $$3}'`="${USER}" ] || \
	sudo chown ${USER}:${USER} ${MONGODBDIR}
	
venv: ${VENV_NAME}/bin/activate
$(VENV_NAME)/bin/activate: setup.py
	[ -d $(VENV_NAME) ] || virtualenv -p python3 $(VENV_NAME)
	${VENV_NAME}/bin/pip3 install -U pip
	${VENV_NAME}/bin/pip3 install -e .
	${VENV_ACTIVATE}

install: venv
	[ -d ${CERTDIR} ] || mkdir -p ${CERTDIR}
	openssl genrsa -out ${CERTDIR}/${SOURCE}_pkey.pem 2048
	openssl req -new -x509 -days 3650 -key ${CERTDIR}/${SOURCE}_pkey.pem \
	-out ${CERTDIR}/${SOURCE}_cert.pem \
	-subj "/C=US/ST=BotCity/L=BotCity/O=Bot Company/CN=${DOMAIN}"
	${PYTHON} setup.py sdist
# 	${PYTHON} setup.py install
# 	make clean
# 	rm -fr *.in *.txt *.py ${SOURCE}

uninstall: 
	make stop-db
	make clean
	make clean-pyc
	rm -fr venv
	rm -fr ${CERTDIR}

run: venv
	[ `docker ps | grep ${MONGODB} | wc -l` -eq 1 ] || \
	make start-db
	${PYTHON} -m ${SOURCE}

start-bot: venv
	[ `docker ps | grep ${MONGODB} | wc -l` -eq 1 ] || \
	make start-db
	screen ${PYTHON} -m ${SOURCE}

start-db:
	docker run --name ${MONGODB} \
	--rm \
	--volume ${MONGODBDIR}:/data/db \
	--publish 127.0.0.1:27017-27019:27017-27019 \
	--detach ${MONGOIMAGE}:latest

stop-db:
	! [ `docker ps | grep ${MONGODB} | wc -l` -eq 1 ] || \
	docker stop ${MONGODB}

remove-db:
	make stop-db
	docker rmi ${MONGOIMAGE}

log-db:
	! [ `docker ps | grep ${MONGODB} | wc -l` -eq 1 ] || \
	docker logs --follow ${MONGODB}

clean:
	rm -fr dist
	rm -fr .eggs
	rm -fr *.egg-info

clean-pyc:
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +

# clean-all: clean clean-pyc
# 	rm -fr build
#  	rm -fr release

# release: clean-all ${SOURCE}
# 	mkdir ${RELEASE}
# 	zip -r ${SOURCE}.zip *.in README.md *.txt *.py *.rules *service ${SOURCE}
# 	zip ${RELEASE}/${SOURCE}-$(shell date '+%Y-%m-%d').zip ${SOURCE}.zip Makefile
# 	rm -fr ${SOURCE}.zip

